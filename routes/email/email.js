const express = require("express");

const Sequelize = require("sequelize");
const router = express.Router();
const Op = Sequelize.Op;


// Display contact form
router.get("/addmember", (req, res) => res.render("addmember"));

// send sms route
router.post("/addmember", (req, res) => {
    // let msg;
    let { name, email, subject, message } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add name" });
    }
    if (!email) {
        errors.push({ text: "Please add email" });
    }
    if (!subject) {
        errors.push({ text: "Please add subject" });
    }
    if (!message) {
        errors.push({ text: "Please add sms" });
    }

    // Check for errors
    if (errors.length > 0) {
        res.render("send", {
            errors,
            name,
            email,
            subject,
            message,
        });
    } else {
        if (!email) {
            email = "Unknown";
        } else {
            email = `${email}`;
        }

        // Make lowercase and remove space after comma
        name = name.toLowerCase().replace(/,[ ]+/g, ",");

        // Insert into table
        Contact.create({
                name,
                email,
                subject,
                message,
            })
            .then((contacts) =>
                res.render("home/home", {
                    layout: false,
                    msg: 'yes',
                })
            )
            .catch((err) => res.render("error", { error: err.message }));
    }
});


module.exports = router;
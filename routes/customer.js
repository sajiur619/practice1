const express = require("express");
const Customer = require("../models/Customer");
const Sequelize = require("sequelize");
const router = express.Router();
const Op = Sequelize.Op;

router.get('/', async(req, res) => {
    res.render('customer', { layout: 'layout' });
})

// send sms route
router.post("/addcustomer", (req, res) => {
    // let msg;
    let { name,email,gender, phone } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add name" });
    }
    if (!email) {
        errors.push({ text: "Please add email" });
    }
    if (!gender) {
        errors.push({ text: "Please add gender" });
    }
    if (!phone) {
        errors.push({ text: "Please add phone" });
    }


    // Check for errors
    if (errors.length > 0) {
        res.render("send", {
            errors,
            name,
            email,
            gender,
             phone
        });
    } else {
        if (!phone) {
            phone = "Unknown";
        } else {
            phone = `${phone}`;
        }

        // Make lowercase and remove space after comma
        name = name.toLowerCase().replace(/,[ ]+/g, ",");

        // Insert into table
        Customer.create({
            name,email,gender, phone
            })
            // .then((members) =>
            //     res.render("customer", {
            //         layout: false,
            //         msg: 'yes',
            //     })
            // )
            .then((customers) => {
                // res.send("successfully updated");
                res.redirect("/customer")
            })

        .catch((err) => res.render("error", { error: err.message }));
    }
});


module.exports = router;
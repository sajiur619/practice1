const Sequelize = require('sequelize');
const db = require('../config/database');

const       Customer = db.define('customers', {
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    gender: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },

});

Customer.sync().then(() => {
    console.log('customers table created');
});
module.exports = Customer;
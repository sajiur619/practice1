const express = require("express");
const Member = require("../models/Member");
const Sequelize = require("sequelize");
const router = express.Router();
const Op = Sequelize.Op;

router.get('/', async(req, res) => {
    res.render('member', { layout: 'layout' });
})

// send sms route
router.post("/addmember", (req, res) => {
    // let msg;
    let { name, phone } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add name" });
    }
    if (!phone) {
        errors.push({ text: "Please add phone" });
    }


    // Check for errors
    if (errors.length > 0) {
        res.render("send", {
            errors,
            name,
            phone
        });
    } else {
        if (!phone) {
            phone = "Unknown";
        } else {
            phone = `${phone}`;
        }

        // Make lowercase and remove space after comma
        name = name.toLowerCase().replace(/,[ ]+/g, ",");

        // Insert into table
        Member.create({
                name,
                phone
            })
            // .then((members) =>
            //     res.render("member", {
            //         layout: false,
            //         msg: 'yes',
            //     })
            // )
            .then((members) => {
                // res.send("successfully updated");
                res.redirect("/member")
            })

        .catch((err) => res.render("error", { error: err.message }));
    }
});


module.exports = router;
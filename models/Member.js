const Sequelize = require('sequelize');
const db = require('../config/database');

const Member = db.define('member', {
    name: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },

});

Member.sync().then(() => {
    console.log('Members table created');
});
module.exports = Member;